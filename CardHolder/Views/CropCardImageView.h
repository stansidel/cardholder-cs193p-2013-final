//
//  CropCardImageView.h
//  CardHolder
//
//  Created by stan on 10/09/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RectsFinder.h"

@interface CropCardImageView : UIView

@property (strong, nonatomic) UIImage *image;
@property (nonatomic) RectPoints rectPoints;

@end
