//
//  CardView.m
//  CardHolder
//
//  Created by stan on 16/09/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "CardView.h"

@implementation CardView

#pragma mark - Properties

- (void)setFrontUp:(BOOL)isFrontUp
{
    _frontUp = isFrontUp;
    [self update];
}

- (void)setFrontImage:(UIImage *)frontImage
{
    _frontImage = frontImage;
    [self update];
}

- (void)setBackImage:(UIImage *)backImage
{
    _backImage = backImage;
    [self update];
}

#pragma mark - Setup

- (void)update
{
    self.contentMode = UIViewContentModeScaleAspectFit;
    
    UIImage *image = self.frontUp ? self.frontImage : self.backImage;
    if (!image) {
        image = [UIImage imageNamed:@"no_image.png"];
    }
    self.image = image;
}

#pragma mark - Gestures

- (void)swipeGesture:(UISwipeGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateRecognized) {
        UIViewAnimationOptions animationOptions = gesture.direction == UISwipeGestureRecognizerDirectionRight ? UIViewAnimationOptionTransitionFlipFromLeft : UIViewAnimationOptionTransitionFlipFromRight;
        [UIView transitionWithView:self
                          duration:0.5
                           options:animationOptions
                        animations:^{
                            self.frontUp = !self.frontUp;
                        }
                        completion:nil
         ];
    }
}

@end
