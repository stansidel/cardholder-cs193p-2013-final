//
//  CardCVC.h
//  CardHolder
//
//  Created by stan on 21/09/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardView.h"

@interface CardCVC : UICollectionViewCell

@property (weak, nonatomic) IBOutlet CardView *cardView;

@end
