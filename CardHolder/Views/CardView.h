//
//  CardView.h
//  CardHolder
//
//  Created by stan on 16/09/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardView : UIImageView

/// If the frontImage is shown
@property (nonatomic) BOOL frontUp;
@property (strong, nonatomic) UIImage *frontImage;
@property (strong, nonatomic) UIImage *backImage;

- (void)swipeGesture:(UISwipeGestureRecognizer *)gesture;

@end
