//
//  CropCardImageView.m
//  CardHolder
//
//  Created by stan on 10/09/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "CropCardImageView.h"

@interface CropCardImageView ()

@end

@implementation CropCardImageView

- (void)setImage:(UIImage *)image
{
    _image = image;
    [self setNeedsDisplay];
}

- (void)setRectPoints:(RectPoints)rectPoints
{
    _rectPoints = rectPoints;
    [self setNeedsDisplay];
}


- (void)drawRect:(CGRect)rect
{
    [[UIBezierPath bezierPathWithRect:rect] addClip];
    
    [[UIColor whiteColor] setFill];
    UIRectFill(rect);
    
    if (self.image) {
        [self.image drawAtPoint:CGPointZero];
        if (![self isRectEmpty:self.rectPoints]) {
            CGContextRef context = UIGraphicsGetCurrentContext();
            [self drawClippingRect:self.rectPoints inContext:context];
        }
    }
}

- (BOOL)isRectEmpty:(RectPoints)rectPoints
{
    BOOL empty = YES;
    empty = empty && (rectPoints.topLeft.x == rectPoints.topRight.x == rectPoints.bottomRight.x == rectPoints.bottomLeft.x);
    empty = empty && (rectPoints.topLeft.y == rectPoints.topRight.y == rectPoints.bottomRight.y == rectPoints.bottomLeft.y);
    return empty;
}

#define HANDLERS_SIZE 30

- (void)drawClippingRect:(RectPoints)rectPoints
               inContext:(CGContextRef)context
{
    UIGraphicsPushContext(context);
    
    CGPoint points[4];
    points[0] = rectPoints.topLeft;
    points[1] = rectPoints.topRight;
    points[2] = rectPoints.bottomRight;
    points[3] = rectPoints.bottomLeft;
    
    UIBezierPath *rectPath = [[UIBezierPath alloc] init];
    [rectPath moveToPoint:rectPoints.topLeft];
    for (int i = 1; i < 4; i++) {
        [rectPath addLineToPoint:points[i]];
    }
    [rectPath closePath];
    
    [[[UIColor grayColor] colorWithAlphaComponent:0.2] setFill];
    [[UIColor greenColor] setStroke];
    rectPath.lineWidth = 3.0;
    
    [rectPath stroke];
    [rectPath fill];
    
    for (int i = 0; i < 4; i++) {
        [self drawHandlerAtPoint:points[i] inContext:context];
    }
    
    UIGraphicsPopContext();
}

- (void)drawHandlerAtPoint:(CGPoint)point inContext:(CGContextRef)context
{
    UIGraphicsPushContext(context);

    CGRect handlerRect = CGRectMake(point.x - HANDLERS_SIZE / 2, point.y - HANDLERS_SIZE / 2, HANDLERS_SIZE, HANDLERS_SIZE);
    UIBezierPath *handler = [UIBezierPath bezierPathWithOvalInRect:handlerRect];
    [[[UIColor magentaColor] colorWithAlphaComponent:0.5] setFill];
    [[UIColor magentaColor] setStroke];
    
    [handler stroke];
    [handler fill];
    
    UIGraphicsPopContext();
}


@end
