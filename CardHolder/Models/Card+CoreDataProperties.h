//
//  Card+CoreDataProperties.h
//  CardHolder
//
//  Created by stan on 16/09/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Card.h"

NS_ASSUME_NONNULL_BEGIN

@interface Card (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *number;
@property (nullable, nonatomic, retain) NSString *frontImageUrl;
@property (nullable, nonatomic, retain) NSString *backImageUrl;
@property (nullable, nonatomic, retain) NSNumber *typeNumber;
@property (nullable, nonatomic, retain) Store *store;

@end

NS_ASSUME_NONNULL_END
