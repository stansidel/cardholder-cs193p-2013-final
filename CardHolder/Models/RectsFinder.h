//
//  RectsFinder.h
//  CardHolder
//
//  Created by stan on 11/09/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CGGeometry.h>
#import <UIKit/UIImage.h>

struct RectPoints
{
    CGPoint topLeft;
    CGPoint topRight;
    CGPoint bottomRight;
    CGPoint bottomLeft;
};
typedef struct RectPoints RectPoints;


@interface RectsFinder : NSObject

@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic, readonly) UIImage *croppedImage;
@property (nonatomic, readonly) RectPoints rectPoints;
@property (nonatomic, readonly) BOOL modifiedByUser;

- (void)movePointNumber:(NSNumber*)number byTranslation:(CGPoint)translation;

@end
