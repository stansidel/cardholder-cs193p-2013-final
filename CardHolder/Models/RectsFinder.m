//
//  RectsFinder.m
//  CardHolder
//
//  Created by stan on 11/09/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "RectsFinder.h"

@implementation RectsFinder

- (void)setImage:(UIImage *)image
{
    if (image != self.image) {
        _image = image;
        _modifiedByUser = NO;
        
        _rectPoints = [self findRectInImage:image];
    }
}

- (void)movePointNumber:(NSNumber*)number byTranslation:(CGPoint)translation
{
    _modifiedByUser = YES;
    switch ([number integerValue]) {
        case 0:
            _rectPoints.topLeft.x += translation.x;
            _rectPoints.topLeft.y += translation.y;
            break;
            
        case 1:
            _rectPoints.topRight.x += translation.x;
            _rectPoints.topRight.y += translation.y;
            break;
            
        case 2:
            _rectPoints.bottomRight.x += translation.x;
            _rectPoints.bottomRight.y += translation.y;
            break;
            
        case 3:
            _rectPoints.bottomLeft.x += translation.x;
            _rectPoints.bottomLeft.y += translation.y;
            break;
            
        default:
            break;
    }

}

- (RectPoints)findRectInImage:(UIImage *)image
{
    RectPoints rectPoints;
    if (image) {
        rectPoints.topLeft = CGPointZero;
        rectPoints.topRight = CGPointMake(image.size.width, 0);
        rectPoints.bottomLeft = CGPointMake(0, image.size.height);
        rectPoints.bottomRight = CGPointMake(image.size.width, image.size.height);
        
        BOOL isRectsDetectionAvailable = (&CIDetectorTypeRectangle != NULL);
        if (isRectsDetectionAvailable) {
            NSDictionary *detectorOptions =
            [NSDictionary dictionaryWithObject:CIDetectorAccuracyHigh
                                        forKey:CIDetectorAccuracy];
            CIDetector *detector = [CIDetector detectorOfType:CIDetectorTypeRectangle
                                                      context:nil
                                                      options:detectorOptions];
            
            NSNumber *orientation = [NSNumber numberWithInt:
                                     [self.image imageOrientation]];
            NSDictionary *imageOptions =
            [NSDictionary dictionaryWithObject:orientation
                                        forKey:CIDetectorImageOrientation];
            
            CIImage *ciimage = [CIImage imageWithCGImage:[self.image CGImage]
                                                 options:imageOptions];
            
            NSArray *features = [detector featuresInImage:ciimage];
            
            NSLog(@"Features count: %lu", (unsigned long)features.count);
            
            CIFeature *feature = nil;
            CGFloat maxArea = 0;
            CGFloat currentArea = 0;
            
            for (CIFeature *currentFeature in features) {
                currentArea = currentFeature.bounds.size.height * currentFeature.bounds.size.width;
                if (currentArea > maxArea) {
                    feature = currentFeature;
                }
            }
            
            if (feature) {
                rectPoints.topLeft = feature.bounds.origin;
                rectPoints.topRight = CGPointMake(feature.bounds.origin.x + feature.bounds.size.width, feature.bounds.origin.y);
                rectPoints.bottomRight = CGPointMake(feature.bounds.origin.x + feature.bounds.size.width, feature.bounds.origin.y + feature.bounds.size.height);
                rectPoints.bottomLeft = CGPointMake(feature.bounds.origin.x, feature.bounds.origin.y + feature.bounds.size.height);
            }
        }
    } else {
        rectPoints.topLeft = rectPoints.topRight = CGPointZero;
        rectPoints.bottomLeft = rectPoints.bottomRight = CGPointZero;
    }
    return rectPoints;
}

- (UIImage *)croppedImage
{
    if (self.image) {
        CGFloat width = MAX(self.rectPoints.topRight.x, self.rectPoints.bottomRight.x) - MIN(self.rectPoints.topLeft.x, self.rectPoints.bottomLeft.x);
        CGFloat height = MAX(self.rectPoints.topRight.y, self.rectPoints.topLeft.y) - MIN(self.rectPoints.bottomLeft.y, self.rectPoints.bottomLeft.y);
        CGRect cropRect = CGRectMake(
                                     MIN(
                                         self.rectPoints.topLeft.x,
                                         self.rectPoints.bottomLeft.x),
                                     MAX(
                                         self.rectPoints.topLeft.y,
                                         self.rectPoints.topRight.y),
                                     ABS(width),
                                     ABS(height));
        return [self cropImage:self.image toRect:cropRect];
    } else {
        return nil;
    }
}

- (UIImage *)cropImage:(UIImage *)image toRect:(CGRect)rect {
    if (image.scale > 1.0f) {
        rect = CGRectMake(rect.origin.x * image.scale,
                          rect.origin.y * image.scale,
                          rect.size.width * image.scale,
                          rect.size.height * image.scale);
    }
    
    CGImageRef imageRef = CGImageCreateWithImageInRect(image.CGImage, rect);
    UIImage *result = [UIImage imageWithCGImage:imageRef scale:image.scale orientation:image.imageOrientation];
    CGImageRelease(imageRef);
    return result;
}

@end
