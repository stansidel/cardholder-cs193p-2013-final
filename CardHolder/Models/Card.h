//
//  Card.h
//  CardHolder
//
//  Created by stan on 16/09/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIKit/UIImage.h>

@class Store;

typedef NS_ENUM(NSInteger, CardType) {
    CardTypeDiscount,
    CardTypeBonus
};

NS_ASSUME_NONNULL_BEGIN

@interface Card : NSManagedObject

@property (nonatomic) NSUInteger type;
@property (weak, nonatomic) UIImage *frontImage;
@property (weak, nonatomic) UIImage *backImage;

@end

NS_ASSUME_NONNULL_END

#import "Card+CoreDataProperties.h"
