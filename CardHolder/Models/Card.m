//
//  Card.m
//  CardHolder
//
//  Created by stan on 16/09/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "Card.h"

@implementation Card

- (void)setType:(NSUInteger)type
{
    self.typeNumber = @(type);
}

- (NSUInteger)type
{
    return [self.typeNumber integerValue];
}

- (void)setFrontImage:(UIImage *)frontImage
{
    NSError *error;
    NSString *newUrl = [self updateImage:frontImage forImagePath:self.frontImageUrl error:&error];
    if (!error) {
        self.frontImageUrl = newUrl;
    }
}

- (void)setBackImage:(UIImage *)backImage
{
    NSError *error;
    NSString *newUrl = [self updateImage:backImage forImagePath:self.backImageUrl error:&error];
    if (!error) {
        self.backImageUrl = newUrl;
    }
}

- (NSString *)updateImage:(UIImage *)image forImagePath:(NSString *)imagePath error:(NSError **)error
{
    if (image) {
        NSError *currentError;
        NSString *newUrl = [self saveImage:image error:&currentError];
        if (newUrl) {
            if (imagePath) {
                NSFileManager *fileManager = [[NSFileManager alloc] init];
                NSError *error;
                if (![fileManager removeItemAtPath:imagePath error:&error]) {
                    NSLog(@"Unable to delete file %@. Error: %@.", imagePath, [error localizedDescription]);
                }
            }
        } else {
            *error = currentError;
        }
        return newUrl;
    } else {
        return nil;
    }
}

- (UIImage *)frontImage
{
    if (self.frontImageUrl) {
        return [UIImage imageWithContentsOfFile:self.frontImageUrl];
    } else {
        return nil;
    }
}

- (UIImage *)backImage
{
    if (self.backImageUrl) {
        return [UIImage imageWithContentsOfFile:self.backImageUrl];
    } else {
        return nil;
    }
}

#pragma mark - Working with images

- (NSString *)imagesPath:(NSError **)error
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    
    NSString *imagesPath = nil;
    if ([paths count] == 1) {
        imagesPath = [[paths objectAtIndex:0] stringByAppendingString:@"/cards_images/"];
        
        BOOL isDir;
        if (![fileManager fileExistsAtPath:imagesPath isDirectory:&isDir] || !isDir) {
            NSError *currentError;
            BOOL imageCacheAvailable = [fileManager createDirectoryAtPath:imagesPath
                                              withIntermediateDirectories:YES
                                                               attributes:nil
                                                                    error:&currentError];
            if (!imageCacheAvailable) {
                imagesPath = nil;
                NSLog(@"Got an error while getting path for cards_images: %@", [currentError localizedDescription]);
                *error = currentError;
            }
        }
    }
    
    return imagesPath;
}

/**
 Saves the new image on the disk.
 @param image Image to save
 @return String with a url of the new
 */
- (NSString *)saveImage:(UIImage *)image error:(NSError **)error
{
    NSError *currentError;
    NSString *imagesPath = [self imagesPath:&currentError];
    
    if (imagesPath) {
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        NSString *guid = [[NSUUID new] UUIDString];
        NSString *fileName = [NSString stringWithFormat:@"%@_%@.png", self.number, guid];
        
        NSString *fullFilePath = [imagesPath stringByAppendingString:fileName];
        if ([fileManager isWritableFileAtPath:fullFilePath] || ![fileManager fileExistsAtPath:fullFilePath]) {
            if ([UIImagePNGRepresentation(image) writeToFile:fullFilePath atomically:YES]) {
                // Removing the previous file
                return fullFilePath;
            } else {
                NSString *errorString = [NSString stringWithFormat:@"Couldn't write image to file with name %@ (writing to %@).", fileName, fullFilePath];
                NSLog(@"%@", errorString);
                NSMutableDictionary *errorUserInfo = [[NSMutableDictionary alloc] init];
                [errorUserInfo setValue:errorString forKey:NSLocalizedDescriptionKey];
                *error = [NSError errorWithDomain:@"card" code:200 userInfo:errorUserInfo];
                return nil;
            }
        } else {
            NSString *errorString = [NSString stringWithFormat:@"Couldn't write image to file with name %@ (writing to %@). File is not writable.", fileName, fullFilePath];
            NSLog(@"%@", errorString);
            NSMutableDictionary *errorUserInfo = [[NSMutableDictionary alloc] init];
            [errorUserInfo setValue:errorString forKey:NSLocalizedDescriptionKey];
            *error = [NSError errorWithDomain:@"card" code:200 userInfo:errorUserInfo];
            return nil;
        }
    } else {
        *error = currentError;
        return nil;
    }
}


@end
