//
//  Store+CoreDataProperties.m
//  CardHolder
//
//  Created by stan on 16/09/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Store+CoreDataProperties.h"

@implementation Store (CoreDataProperties)

@dynamic name;
@dynamic cards;

@end
