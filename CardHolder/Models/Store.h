//
//  Store.h
//  CardHolder
//
//  Created by stan on 16/09/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Card;

NS_ASSUME_NONNULL_BEGIN

@interface Store : NSManagedObject

+ (Store *)storeWithName:(NSString *)name inManagedObjectContext:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "Store+CoreDataProperties.h"
