//
//  Store.m
//  CardHolder
//
//  Created by stan on 16/09/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "Store.h"
#import "Card.h"

@implementation Store

+ (Store *)storeWithName:(NSString *)name inManagedObjectContext:(NSManagedObjectContext *)context
{
    NSString *capitalizedName = [name capitalizedString];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Store"];
    request.predicate = [NSPredicate predicateWithFormat:@"name = %@", capitalizedName];
    NSError *error;
    NSArray *stores = [context executeFetchRequest:request error:&error];

    Store *store = nil;
    if (!stores) {
        NSLog(@"Unable to fetch stores for name %@. Error: %@.", capitalizedName, [error localizedDescription]);
    } else {
        store = [stores firstObject];
        store.name = capitalizedName;
    }
    
    if (!store) {
        store = [NSEntityDescription insertNewObjectForEntityForName:@"Store" inManagedObjectContext:context];
    }
    
    return store;
}

@end
