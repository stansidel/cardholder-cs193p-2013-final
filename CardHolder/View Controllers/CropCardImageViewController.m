//
//  CropCardImageViewController.m
//  CardHolder
//
//  Created by stan on 10/09/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "CropCardImageViewController.h"
#import "CropCardImageView.h"
#import "RectsFinder.h"

@interface CropCardImageViewController ()

@property (weak, nonatomic) IBOutlet CropCardImageView *cropImageView;
@property (strong, nonatomic) RectsFinder *rectsFinder;
@property (nonatomic) RectPoints scaledRectPoints;
@property (nonatomic) CGFloat scaleFactor;
@property (nonatomic) NSNumber *selectedHandler;

@end

@implementation CropCardImageViewController

- (RectsFinder *)rectsFinder
{
    if (!_rectsFinder) _rectsFinder = [[RectsFinder alloc] init];
    return _rectsFinder;
}

- (void)setImage:(UIImage *)image
{
    _image = image;
    self.rectsFinder.image = image;
    self.scaledRectPoints = self.rectsFinder.rectPoints;
    [self updateImageView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateImageView];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateImageView];
}

#define HANDLER_TOUCH_AREA 20

- (IBAction)moveHandler:(UIPanGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        CGPoint currentPoint = [recognizer locationInView:self.cropImageView];
        CGPoint translation = [recognizer translationInView:self.cropImageView];
        CGPoint originalPoint = currentPoint;
        originalPoint.x -= translation.x;
        originalPoint.y -= translation.y;

        CGPoint points[] = {self.scaledRectPoints.topLeft, self.scaledRectPoints.topRight, self.scaledRectPoints.bottomRight, self.scaledRectPoints.bottomLeft};
        for (int i = 0; i < 4; i++) {
            self.selectedHandler = nil;
            if ((ABS(points[i].x - originalPoint.x) <= HANDLER_TOUCH_AREA) &&
                (ABS(points[i].y - originalPoint.y) <= HANDLER_TOUCH_AREA)) {
                self.selectedHandler = [[NSNumber alloc] initWithInt:i];
                break;
            }
        }
    }
    if ((recognizer.state == UIGestureRecognizerStateChanged) ||
        (recognizer.state == UIGestureRecognizerStateEnded)) {
        if (self.selectedHandler) {
            CGPoint translation = [recognizer translationInView:self.cropImageView];
            
            if (self.scaleFactor != 0) {
                translation.x /= self.scaleFactor;
                translation.y /= self.scaleFactor;
            }
            
            [self.rectsFinder movePointNumber:self.selectedHandler byTranslation:translation];
            [self updateImageView];
        }
        [recognizer setTranslation:CGPointZero inView:self.cropImageView];
    }
    
    if ((recognizer.state == UIGestureRecognizerStateCancelled) ||
        (recognizer.state == UIGestureRecognizerStateEnded) ||
        (recognizer.state == UIGestureRecognizerStateFailed)) {
        self.selectedHandler = nil;
    }
}

#pragma mark - Prepare image

- (void)updateImageView
{
//    self.cropImageView.rectsFinder = self.rectsFinder;
    if (self.cropImageView) {
        UIImage *image = self.rectsFinder.image;
        self.scaleFactor = [self scaleFactorFitImage:image toSize:self.cropImageView.bounds.size];
        
        UIImage *scaledImage = [self imageScaled:image toSize:CGSizeMake(image.size.width * self.scaleFactor, image.size.height * self.scaleFactor)];
        self.cropImageView.image = scaledImage;
        
        RectPoints scaledRectPoints = self.rectsFinder.rectPoints;
        scaledRectPoints.topLeft.x *= self.scaleFactor;
        scaledRectPoints.topLeft.y *= self.scaleFactor;
        scaledRectPoints.topRight.x *= self.scaleFactor;
        scaledRectPoints.topRight.y *= self.scaleFactor;
        scaledRectPoints.bottomRight.x *= self.scaleFactor;
        scaledRectPoints.bottomRight.y *= self.scaleFactor;
        scaledRectPoints.bottomLeft.x *= self.scaleFactor;
        scaledRectPoints.bottomLeft.y *= self.scaleFactor;
        self.scaledRectPoints = scaledRectPoints;
        
        self.cropImageView.rectPoints = scaledRectPoints;
    }
}

// See http://stackoverflow.com/a/8858464/758990

- (UIImage *)imageScaled:(UIImage*)image toSize:(CGSize)size
{
    //create drawing context
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);
    
    //draw
    [image drawInRect:CGRectMake(0.0f, 0.0f, size.width, size.height)];
    
    //capture resultant image
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //return image
    return resultImage;
}

- (CGFloat)scaleFactorFitImage:(UIImage *)image toSize:(CGSize)size
{
    CGFloat scaleFactor = 1;
    if (image.size.width > 0 && image.size.height > 0) {
        CGFloat scaleWidth = size.width / image.size.width;
        CGFloat scaleHeight = size.height / image.size.height;
        scaleFactor *= MIN(scaleWidth, scaleHeight);
    }
    return scaleFactor;
}

#pragma mark - Navigation

- (UIImage *)croppedImage
{
    return [self.rectsFinder croppedImage];
}

@end
