//
//  CardEditViewController.h
//  CardHolder
//
//  Created by Stanislav Sidelnikov on 07/09/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Card.h"

@interface CardEditViewController : UITableViewController

@property (strong, nonatomic) Card *card;

@end
