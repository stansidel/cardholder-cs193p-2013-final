//
//  CardsViewController.m
//  CardHolder
//
//  Created by Stanislav Sidelnikov on 07/09/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "CardsViewController.h"
#import "CardEditViewController.h"
#import "Card.h"
#import "AppDelegate.h"
#import "CardCVC.h"

@interface CardsViewController ()

@end

@implementation CardsViewController

- (void)viewDidLoad
{
    [self setupFetchedResultsController];
}

#pragma mark - Properties

- (void)setupFetchedResultsController
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Card"];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"store.name" ascending:YES];
    request.sortDescriptors = @[sortDescriptor];
    request.predicate = nil;
    
    NSManagedObjectContext *context = [(AppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    NSError *error;
    NSArray *cards = [context executeFetchRequest:request error:&error];
    
    NSLog(@"Found %lu cards.", (unsigned long)[cards count]);
    
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    self.fetchedResultsController = fetchedResultsController;
}

#pragma mark - Navigation

- (IBAction)cancelEditing:(UIStoryboardSegue *)segue
{
    
}

- (IBAction)finishAdding:(UIStoryboardSegue *)segue
{
    
}

- (IBAction)cardDeleted:(UIStoryboardSegue *)sender
{
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Edit Card"]) {
        if ([sender isKindOfClass:[UIGestureRecognizer class]]) {
            UIGestureRecognizer *gesture = (UIGestureRecognizer *)sender;
            CGPoint gesturePoint = [gesture locationInView:self.collectionView];
            NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:gesturePoint];
            if (indexPath && (gesture.state == UIGestureRecognizerStateRecognized)) {
                Card *card = [self.fetchedResultsController objectAtIndexPath:indexPath];
                if (card) {
                    UIViewController *controller = segue.destinationViewController;
                    if ([controller isKindOfClass:[CardEditViewController class]]) {
                        CardEditViewController *cardEditVC = (CardEditViewController *)controller;
                        cardEditVC.card = card;
                    }
                }
            }
        }
    }
}

#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Card" forIndexPath:indexPath];
    if ([cell isKindOfClass:[CardCVC class]]) {
        CardCVC *cardCVC = (CardCVC *)cell;
        Card *card = [self.fetchedResultsController objectAtIndexPath:indexPath];
        cardCVC.cardView.frontImage = card.frontImage;
        cardCVC.cardView.backImage = card.backImage;
        cardCVC.cardView.frontUp = YES;
    }
    return cell;

}


#pragma mark - Gesture recognizers

- (IBAction)cardSwipeGestureRecognizer:(UISwipeGestureRecognizer *)gesture
{
    CGPoint gesturePoint = [gesture locationInView:self.collectionView];
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:gesturePoint];
    if (indexPath && (gesture.state == UIGestureRecognizerStateRecognized)) {
        UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
        if (cell && [cell isKindOfClass:[CardCVC class]]) {
            CardCVC *cardCVC = (CardCVC *)cell;
            [cardCVC.cardView swipeGesture:gesture];
        }
    }
}

@end
