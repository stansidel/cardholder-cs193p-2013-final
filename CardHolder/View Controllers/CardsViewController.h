//
//  CardsViewController.h
//  CardHolder
//
//  Created by Stanislav Sidelnikov on 07/09/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataCollectionViewController.h"

@interface CardsViewController : CoreDataCollectionViewController

@end
