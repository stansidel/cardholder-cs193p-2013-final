//
//  CropCardImageViewController.h
//  CardHolder
//
//  Created by stan on 10/09/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CropCardImageViewController : UIViewController

@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic, readonly) UIImage *croppedImage;

@end
