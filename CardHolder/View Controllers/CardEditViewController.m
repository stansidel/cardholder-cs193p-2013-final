//
//  CardEditViewController.m
//  CardHolder
//
//  Created by Stanislav Sidelnikov on 07/09/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "CardEditViewController.h"
#import "CropCardImageViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "CardView.h"
#import "Store.h"
#import "AppDelegate.h"

@interface CardEditViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIPopoverController *imagePickerPopover;
@property (weak, nonatomic) IBOutlet CardView *cardView;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;

@property (weak, nonatomic) IBOutlet UITextField *cardNumberField;
@property (weak, nonatomic) IBOutlet UITextField *storeNameField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *cardTypeSegmentedControl;
@property (weak, nonatomic) IBOutlet UIToolbar *bottomToolbar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bottomToolbarCancelButton;
@property (weak, nonatomic) IBOutlet UITableViewCell *deleteTableViewCell;

@property (strong, nonatomic) UIImage *cardImageFront;
@property (strong, nonatomic) UIImage *cardImageBack;

@end

@implementation CardEditViewController

#pragma mark - Properties

- (void)setCard:(Card *)card
{
    _card = card;
    [self setupCard];
}

- (void)setCardImageFront:(UIImage *)cardImageFront
{
    _cardImageFront = cardImageFront;
    self.cardView.frontImage = cardImageFront;
}

- (void)setCardImageBack:(UIImage *)cardImageBack
{
    _cardImageBack = cardImageBack;
    self.cardView.backImage = cardImageBack;
}

#pragma mark - Setup

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateWidthForLabels:self.labels];
    [self setupCard];
    self.cardView.frontUp = YES;
    if (![self isModal]) {
        [self removeCancelButton];
    }
}

- (void)removeCancelButton
{
    NSMutableArray *buttons = [self.bottomToolbar.items mutableCopy];
    [buttons removeObjectAtIndex:[self.bottomToolbar.items indexOfObject:self.bottomToolbarCancelButton]];
    [self.bottomToolbar setItems:buttons];
}

- (void)setupCard
{
    self.cardNumberField.text = self.card.number;
    self.storeNameField.text = self.card.store.name;
    if (self.card.type < self.cardTypeSegmentedControl.numberOfSegments) {
        self.cardTypeSegmentedControl.selectedSegmentIndex = self.card.type;
    } else {
        self.cardTypeSegmentedControl.selectedSegmentIndex = 0;
    }
    self.cardImageFront = self.card.frontImage;
    self.cardImageBack = self.card.backImage;
    
    self.deleteTableViewCell.hidden = !self.card;
}

- (BOOL)isModal {
    return self.presentingViewController.presentedViewController == self
    || (self.navigationController != nil && self.navigationController.presentingViewController.presentedViewController == self.navigationController)
    || [self.tabBarController.presentingViewController isKindOfClass:[UITabBarController class]];
}

#pragma mark - Gestures

- (IBAction)imageViewTap:(UITapGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateRecognized) {
        [self presentImagePicker:UIImagePickerControllerSourceTypeCamera sender:self.cardView];
    }
}

- (IBAction)flipCard:(UISwipeGestureRecognizer *)gesture
{
    [self.cardView swipeGesture:gesture];
}

#pragma mark - Camera controls

- (void)presentImagePicker:(UIImagePickerControllerSourceType)sourceType sender:(id)sender
{
    if (!self.imagePickerPopover && [UIImagePickerController isSourceTypeAvailable:sourceType]) {
        NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType];
        if ([availableMediaTypes containsObject:(NSString *)kUTTypeImage]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.sourceType = sourceType;
            picker.mediaTypes = @[(NSString *)kUTTypeImage];
            picker.showsCameraControls = YES;
            picker.allowsEditing = NO;
            picker.delegate = self;
            if ((sourceType != UIImagePickerControllerSourceTypeCamera) && (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)) {
                self.imagePickerPopover = [[UIPopoverController alloc] initWithContentViewController:picker];
                [self.imagePickerPopover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                self.imagePickerPopover.delegate = self;
            } else {
                [self presentViewController:picker animated:YES completion:nil];
            }

        }
    }
}

// popover was canceled so clear out our property that points to the popover

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.imagePickerPopover = nil;
}

// UIImagePickerController was canceled, so dismiss it

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    if (!image) image = info[UIImagePickerControllerOriginalImage];
    if (self.imagePickerPopover) {
        [self.imagePickerPopover dismissPopoverAnimated:YES];
        self.imagePickerPopover = nil;
        // Edit image in popover
        [NSException raise:@"Not implemented" format:@"editting image in a popover (for iPads) is not implemented"];
    } else {
        [self dismissViewControllerAnimated:YES completion:^{
            if (image) {
                // Process the image
                [self performSegueWithIdentifier:@"Edit Image" sender:image];
            }
        }];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Edit Image"]) {
        if ([sender isKindOfClass:[UIImage class]]) {
            if ([segue.destinationViewController respondsToSelector:@selector(setImage:)]) {
                [segue.destinationViewController performSelector:@selector(setImage:) withObject:sender];
            }
        }
    }
    
}

- (void)saveCard
{
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    if (!_card) {
        _card = [NSEntityDescription insertNewObjectForEntityForName:@"Card" inManagedObjectContext:context];
    }
    self.card.number = self.cardNumberField.text;
    self.card.type = self.cardTypeSegmentedControl.selectedSegmentIndex;
    self.card.frontImage = self.cardImageFront;
    self.card.backImage = self.cardImageBack;
    if (self.storeNameField.text) {
        self.card.store = [Store storeWithName:self.storeNameField.text inManagedObjectContext:context];
    } else {
        self.card.store = nil;
    }
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Unable to save context for card. Error: %@.", [error localizedDescription]);
    }
}

- (IBAction)saveButtonPress:(UIBarButtonItem *)sender {
    [self saveCard];
    [self performSegueWithIdentifier:@"Finish Editing" sender:self];
}

- (IBAction)cancelEditImage:(UIStoryboardSegue *)segue
{
    
}

- (IBAction)finishEditImage:(UIStoryboardSegue *)segue
{
    CropCardImageViewController *cropCardImageVC = (CropCardImageViewController *)segue.sourceViewController;
    UIImage *croppedImage = cropCardImageVC.croppedImage;
    if (croppedImage) {
        // Set the image for the card
        if (self.cardView.frontUp) {
            self.cardImageFront = croppedImage;
        } else {
            self.cardImageBack = croppedImage;
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == alertView.firstOtherButtonIndex) {
        // Yes button clicked
        if (self.card) {
            NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
            [context deleteObject:self.card];
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Unable to save context when deleting card. Error: %@.", [error localizedDescription]);
            }
            [self performSegueWithIdentifier:@"Card Deleted" sender:self];
        }
    }
}

- (IBAction)deleteCard:(UIButton *)sender {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Delete card?"
                                                        message:@"Do you really want to delete this card?"
                                                       delegate:self
                                              cancelButtonTitle:@"No"
                                              otherButtonTitles:@"Yes", nil];
    [alertView show];
}

#pragma mark - Visual table view

- (CGFloat)calculateLabelWidth:(UILabel *)label
{
    CGSize labelSize = [label sizeThatFits:CGSizeMake(CGFLOAT_MAX, label.frame.size.height)];
    
    return labelSize.width;
}

- (CGFloat)calculateMaxLabelWidth:(NSArray *)labels
{
    CGFloat maxWidth = 0;
    for (UILabel *label in labels) {
        maxWidth = MAX([self calculateLabelWidth:label], maxWidth);
    }
    
    return maxWidth;
}

- (void)updateWidthForLabels:(NSArray *)labels
{
    CGFloat maxLabelWidth = [self calculateMaxLabelWidth:labels];
    for (UILabel *label in labels) {
        NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:label
                                     attribute:NSLayoutAttributeWidth
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:nil
                                     attribute:NSLayoutAttributeNotAnAttribute
                                    multiplier:1
                                      constant:maxLabelWidth];
        [label addConstraint:constraint];
    }
}

@end
